﻿using UnityEngine;

namespace Fortify.InventorySystem {
    public class Item : MonoBehaviour {
        public string Name;
        public int Id;
        public bool Buildable;
        public GameObject Model;
        public GameObject BuildModel;
        public Sprite Icon;
    }
}