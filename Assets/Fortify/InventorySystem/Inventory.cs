﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Fortify.InventorySystem {
    public class Inventory : MonoBehaviour {
        public int[] Items;
        public GameObject ItemFrame;
        public GameObject ItemView;
        public GameObject InvPanel;

        private List<GameObject> _itemFrames;

        public void Enable() {
            Items = new int[MatchInitialiser.Instance.Items.Length];
            _itemFrames = new List<GameObject>();

            for (var i = 0; i < Items.Length; i++) {
                Items[i] = 0;
            }
        }

        private void Update() {
            if (_itemFrames == null) {
                return;
            }

            if (Input.GetButtonDown("Inventory")) {
                InvPanel.SetActive(!InvPanel.activeInHierarchy);
                InputManager.ToggleMouseLock();
            }

            for (var i = 0; i < Items.Length; i++) {
                var need = true;
                foreach (var iFrame in _itemFrames) {
                    if (iFrame.GetComponent<ItemFrame>().Item.Id == i) {
                        need = false;
                    }
                }

                if (need) {
                    if (Items[i] <= 0) continue;
                    var itemRender = Instantiate(ItemFrame, Vector3.zero, Quaternion.Euler(Vector3.zero),
                        ItemView.transform) as GameObject;
                    if (itemRender == null) continue;
                    itemRender.GetComponent<ItemFrame>().Item = MatchInitialiser.Instance.Items[i].GetComponent<Item>();
                    itemRender.transform.localPosition = Vector3.zero;
                    itemRender.transform.FindChild("Icon").GetComponent<Image>().sprite =
                        MatchInitialiser.Instance.Items[i].GetComponent<Item>().Icon;
                    itemRender.transform.FindChild("ItemName").GetComponent<Text>().text =
                        MatchInitialiser.Instance.Items[i].GetComponent<Item>().Name;
                    itemRender.transform.FindChild("ItemAmount").GetComponent<Text>().text = Items[i] + "x";
                    _itemFrames.Add(itemRender);
                } else {
                    if (Items[i] > 0) continue;
                    foreach (var iFrame in _itemFrames) {
                        if (iFrame.GetComponent<ItemFrame>().Item.Id != i) continue;
                        _itemFrames.Remove(iFrame);
                        Destroy(iFrame);
                        return;
                    }
                }
            }

            var offset = -50;
            foreach (var iFrame in _itemFrames) {
                iFrame.transform.FindChild("ItemAmount").GetComponent<Text>().text =
                    Items[iFrame.GetComponent<ItemFrame>().Item.Id] + "x";
                iFrame.transform.localPosition = new Vector3(250, offset, 0);
                offset -= 95;
            }
            ItemView.GetComponent<RectTransform>().sizeDelta = new Vector2(0.0F, -offset - 45);
        }

        public void AddItems(int id, int amount) {
            Items[id] = Items[id] + amount;
        }
    }
}