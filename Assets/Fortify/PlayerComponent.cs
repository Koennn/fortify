﻿using UnityEngine;

namespace Fortify {
    public class PlayerComponent : MonoBehaviour {
        public string Name;
        public string Character;
        public float MaxHealth;
        public float Health;
        public bool TookDamage;

        private float _lastHealth;

        private void Start() {
            _lastHealth = Health;
        }

        private void Update() {
            if (_lastHealth != Health) {
                TookDamage = true;
            } else if (TookDamage) {
                TookDamage = false;
            }
            _lastHealth = Health;
        }

        public void Damage(float amount) {
            Health -= amount;
        }

        public void Heal(float amount) {
            Health += amount;
        }
    }
}