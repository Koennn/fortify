﻿using UnityEngine;

namespace Fortify.Characters {
    public class Character : MonoBehaviour {
        public string Name;
        [Range(1, 5)] public int SkillLevel = 1;
        public Sprite Icon;
        public GameObject Prefab;
        public GameObject Weapon;
        public GameObject[] Abilities;

        private void Start() {
            Debug.Log("Hey there! " + Name + " is loaded and ready to go!");
        }
    }
}