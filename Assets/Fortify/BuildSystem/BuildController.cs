﻿using UnityEngine;

namespace Fortify.BuildSystem {
    public class BuildController : MonoBehaviour {
        public GameObject TestPrefab;

        private bool _building;
        private GameObject _selected;

        private void Start() { }

        private void Update() {
            if (Input.GetButtonDown("Start")) {
                _building = !_building;
            }

            if (_building) {
                if (_selected == null) {
                    _selected = (GameObject) Instantiate(TestPrefab, new Vector3(0, 0, 0), new Quaternion());
                }

                RaycastHit hit;
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Debug.DrawRay(ray.origin, ray.direction, Color.red);
                if (Physics.Raycast(ray, out hit)) {
                    var lookPos = hit.point;
                    _selected.transform.position = new Vector3(Mathf.Round(lookPos.x), lookPos.y - 0.05F,
                        Mathf.Round(lookPos.z));
                    Debug.Log(hit);
                } else {
                    Debug.Log(false);
                }
            } else {
                if (_selected == null) return;

                Destroy(_selected);
                _selected = null;
            }
        }
    }
}