﻿using System;
using Fortify.InventorySystem;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Fortify.BuildSystem {
    public class TreeController : MonoBehaviour {
        public TreeSize Size;
        public float Health;

        private GameObject _fpsController;

        private void Start() {
            _fpsController = GameObject.Find("FPSController");
            Health = (float) Size;
        }

        public void Chop() {
            var inv = _fpsController.GetComponent<Inventory>();

            int amount;
            switch (Size) {
                case TreeSize.Small:
                    amount = (int) Mathf.Round(Random.Range(1, 2));
                    break;
                case TreeSize.Medium:
                    amount = (int) Mathf.Round(Random.Range(3, 4));
                    break;
                case TreeSize.Large:
                    amount = (int) Mathf.Round(Random.Range(5, 6));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            inv.AddItems((int) MatchInitialiser.CommonIDs.Wood, amount);
            Remove();
        }

        public void Remove() {
            Destroy(gameObject);
        }

        public enum TreeSize {
            Small = 6,
            Medium = 12,
            Large = 24
        }
    }
}