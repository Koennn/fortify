﻿using UnityEngine;
using UnityEngine.UI;

namespace Fortify {
    public class HealthRenderer : MonoBehaviour {
        public Slider HealthBarSlider;
        public Text HealthText;

        public float CurrentHealth = 100;
        public float MaxHealth = 100;

        private void Update() {
            HealthBarSlider.value = CurrentHealth / MaxHealth;
            HealthText.text = Mathf.Round(CurrentHealth) + "/" + MaxHealth;
        }
    }
}