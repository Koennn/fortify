﻿using Fortify.Characters;
using Fortify.EventSystem;
using Fortify.EventSystem.Events;
using Fortify.Hud;
using Fortify.InventorySystem;
using UnityEngine;

namespace Fortify {
    public class MatchInitialiser : MonoBehaviour {
        public static MatchInitialiser Instance;

        public GameObject Runtime;
        public string SelectedCharacter;
        public GameObject[] Characters;
        public HudPlayer[] Players;
        public GameObject Hud;
        public GameObject FpsController;
        public AudioClip HitSound;
        public GameObject[] Items;

        [HideInInspector] public GameObject Character;

        public MatchInitialiser() {
            Instance = this;
        }

        /// <summary>
        /// Start the MatchInitializer to initialize the match.
        /// </summary>
        private void Start() {
            //Call the MatchInitializingEvent.
            var startEvent = new MatchInitializingEvent(this);
            EventManager.CallEvent(startEvent);

            //Load fake players.
            Players = new HudPlayer[12];
            for (var i = 0; i < 12; i++) {
                Players[i] = new HudPlayer(UsernameGenerator.Generate(), Random.value > 0.5F);
            }

            //Load selected character
            GameObject selected = null;
            foreach (var pchar in Characters) {
                if (pchar.GetComponent<Character>().Name.Equals(SelectedCharacter)) {
                    selected = pchar;
                }
            }

            //Instantiate character prefab.
            Character = Instantiate(selected);
            Character.transform.SetParent(Runtime.transform);

            //Enable the Hud.
            Hud.GetComponent<HudController>().Enable();

            //Enable the Inventory
            FpsController.GetComponent<Inventory>().Enable();
        }

        private void Update() {
            // ReSharper disable once InvertIf
            if (Input.GetKeyDown(KeyCode.V)) {
                FpsController.GetComponent<Inventory>().AddItems((int) CommonIDs.Wood, 1);
                Debug.Log(FpsController.transform.position);
            }
        }

        public enum CommonIDs {
            Wood = 0
        }

        private void Awake() {
            //Call the MatchInitializedEvent.
            var startEvent = new MatchInitializedEvent(this);
            EventManager.CallEvent(startEvent);
        }
    }
}