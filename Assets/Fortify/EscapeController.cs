﻿using UnityEngine;

namespace Fortify {
    public class EscapeController : MonoBehaviour {
        public GameObject EscapeOverlay;

        private void Start() {
            EscapeOverlay.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void Update() {
            if (!Input.GetButtonDown("Cancel")) return;

            InputManager.ToggleMouseLock();
            var active = !EscapeOverlay.activeSelf;
            EscapeOverlay.SetActive(active);
        }
    }
}