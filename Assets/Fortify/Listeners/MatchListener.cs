﻿using Fortify.EventSystem;
using Fortify.EventSystem.Events;
using UnityEngine;

namespace Fortify.Listeners {
    public class MatchListener : EventAdapter {
        public override void OnMatchInitializing(MatchInitializingEvent e) {
            Debug.Log(e);
        }

        public override void OnMatchInitialized(MatchInitializedEvent e) {
            Debug.Log(e);
        }
    }
}