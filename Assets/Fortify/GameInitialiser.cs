﻿using Fortify.EventSystem;
using UnityEngine;

namespace Fortify {
    public class GameInitialiser {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Startup() {
            Debug.Log("Starting game...");
            EventManager.RegisterKnownEvents();
        }
    }
}