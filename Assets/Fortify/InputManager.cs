﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace Fortify {
    public class InputManager {
        private static MouseLook _mouseLook;
        public static bool Unlocked = true;

        /// <summary>
        /// Toggle the lock on the mouse on or off
        /// </summary>
        public static void ToggleMouseLock() {
            if (_mouseLook == null) {
                _mouseLook = GameObject.Find("FPSController").GetComponent<FirstPersonController>().m_MouseLook;
            }

            if (Unlocked) {
                DisableLock();
            } else {
                EnableLock();
            }
            GameObject.Find("FPSController").GetComponent<FirstPersonController>().locked = !Unlocked;
        }

        /// <summary>
        /// Enable the mouse lock
        /// </summary>
        private static void EnableLock() {
            Unlocked = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            _mouseLook.lockCursor = true;
        }

        /// <summary>
        /// Disable the mouse lock
        /// </summary>
        private static void DisableLock() {
            Unlocked = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            _mouseLook.lockCursor = false;
        }
    }
}