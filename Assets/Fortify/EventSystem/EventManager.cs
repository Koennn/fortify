﻿using System.Collections.Generic;
using Fortify.Listeners;

namespace Fortify.EventSystem {
    public class EventManager {
        private static readonly List<EventAdapter> EventAdapters = new List<EventAdapter>();

        public static void CallEvent(Event e) {
            foreach (var adapter in EventAdapters) {
                var adapterType = adapter.GetType();
                var eventMethod = adapterType.GetMethod("On" + e.Name.Replace("Event", ""));
                eventMethod.Invoke(adapter, new object[] {e});
            }
        }

        public static void RegisterKnownEvents() {
            EventAdapters.Add(new MatchListener());
        }
    }
}