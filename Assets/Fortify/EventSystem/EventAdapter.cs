﻿using Fortify.EventSystem.Events;

namespace Fortify.EventSystem {
    public class EventAdapter {
        public virtual void OnMatchInitializing(MatchInitializingEvent e) { }

        public virtual void OnMatchInitialized(MatchInitializedEvent e) { }
    }
}