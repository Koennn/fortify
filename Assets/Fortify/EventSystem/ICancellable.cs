﻿namespace Fortify.EventSystem {
    public interface ICancellable {
        bool IsCancelled();

        void SetCancelled();
    }
}