﻿namespace Fortify.EventSystem.Events {
    public class MatchInitializingEvent : Event {
        public MatchInitialiser MatchInitialiser { get; private set; }

        public MatchInitializingEvent(MatchInitialiser matchInitialiser) : base("MatchInitializingEvent") {
            MatchInitialiser = matchInitialiser;
        }
    }
}