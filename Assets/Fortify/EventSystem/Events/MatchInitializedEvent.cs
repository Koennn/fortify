﻿namespace Fortify.EventSystem.Events {
    public class MatchInitializedEvent : Event {
        public MatchInitialiser MatchInitialiser { get; private set; }

        public MatchInitializedEvent(MatchInitialiser matchInitialiser) : base("MatchInitializedEvent") {
            MatchInitialiser = matchInitialiser;
        }
    }
}