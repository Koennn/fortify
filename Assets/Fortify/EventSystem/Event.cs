﻿namespace Fortify.EventSystem {
    public class Event {
        public string Name { get; private set; }

        protected Event(string name) {
            Name = name;
        }
    }
}