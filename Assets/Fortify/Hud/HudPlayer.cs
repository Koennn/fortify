﻿namespace Fortify.Hud {
    public class HudPlayer {
        public string Name;
        public bool Dead;

        public HudPlayer(string name, bool dead) {
            Name = name;
            Dead = dead;
        }
    }
}