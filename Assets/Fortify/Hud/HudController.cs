﻿using Fortify.Abilities;
using Fortify.Characters;
using UnityEngine;
using UnityEngine.UI;

namespace Fortify.Hud {
    public class HudController : MonoBehaviour {
        public Slider HealthBarSlider;
        public Text HealthText;
        public Text AmmoCount;
        public HudPlayer[] Teams;
        public GameObject[] TeamFrames;
        public GameObject Abilities;
        public GameObject AbilityPrefab;

        [HideInInspector] public Character Character;

        public float CurrentHealth = 100;
        public float MaxHealth = 100;

        public void Enable() {
            Character = MatchInitialiser.Instance.Character.GetComponent<Character>();
            Teams = MatchInitialiser.Instance.Players;

            var players = new Text[12];
            var index = 0;
            foreach (Transform child in TeamFrames[0].transform) {
                if (!child.gameObject.name.StartsWith("Player")) continue;
                players[index] = child.gameObject.GetComponent<Text>();
                index++;
            }
            foreach (Transform child in TeamFrames[1].transform) {
                if (!child.gameObject.name.StartsWith("Player")) continue;
                players[index] = child.gameObject.GetComponent<Text>();
                index++;
            }

            index = 0;
            foreach (var player in players) {
                player.text = Teams[index].Name;
                index++;
            }

            foreach (var player in players) {
                var icon = player.gameObject.transform.FindChild("Icon").gameObject.GetComponent<Image>();
                var deadIndicator = player.gameObject.transform.FindChild("DeadIndicator").gameObject;
                deadIndicator.transform.localPosition = new Vector3(0, 900, 0);
            }

            var offset = 0;
            foreach (var ability in Character.Abilities) {
                if (ability.GetComponent<Ability>() == null) {
                    continue;
                }

                var abilityObj = Instantiate(AbilityPrefab);
                abilityObj.transform.parent = Abilities.transform;
                abilityObj.transform.localPosition = new Vector3(offset, 0, 0);
                offset += 110;

                var hotkey = abilityObj.transform.FindChild("KeyLayout")
                    .FindChild("Key")
                    .gameObject.GetComponent<Text>();
                hotkey.text = ability.GetComponent<Ability>().Hotkey;
                var icon = abilityObj.transform.FindChild("Icon").gameObject.GetComponent<Image>();
                icon.sprite = ability.GetComponent<Ability>().Icon;
            }
        }

        private void Update() {
            if (Teams == null || Teams.Length == 0) {
                return;
            }

            HealthBarSlider.value = CurrentHealth / MaxHealth;
            HealthText.text = Mathf.Round(CurrentHealth) + "/" + MaxHealth;

            var index = 0;

            for (var i = 0; i < 2; i++) {
                foreach (Transform child in TeamFrames[i].transform) {
                    if (!child.gameObject.name.StartsWith("Player")) continue;

                    var deadInd = child.FindChild("DeadIndicator").gameObject;
                    if (Teams[index].Dead) {
                        if (deadInd.transform.localPosition.y == 900) {
                            deadInd.transform.localPosition = new Vector3(0, 90, 0);
                        }
                    } else {
                        if (deadInd.transform.localPosition.y == 90) {
                            deadInd.transform.localPosition = new Vector3(0, 900, 0);
                        }
                    }
                    index++;
                }
            }
        }
    }
}