﻿using System.Collections;
using Fortify.Hud;
using UnityEngine;

namespace Fortify.Abilities {
    public class HitscanWeapon : MonoBehaviour, IAbilityExecutor {
        public float Damage;
        public float Spread;
        public float Falloff;
        public float FireRate;
        public int MaxAmmo;
        public int Ammo;
        public int AmmoUse;
        public int BulletsPerShot;

        public AudioClip ShootSound;
        public AudioClip ReloadSound;

        public GameObject HitParticle;

        private AudioSource _audioSource;
        private float _lastShot;

        private void Start() {
            _audioSource = GetComponent<AudioSource>();
            if (_audioSource != null) {
                _audioSource.playOnAwake = false;
            }
        }

        private void Update() {
            if (Input.GetKeyDown(KeyCode.R) && InputManager.Unlocked) {
                if (_audioSource != null && ReloadSound != null) {
                    _audioSource.clip = ReloadSound;
                    _audioSource.Play();
                    StartCoroutine("Reload");
                }
            }

            GameObject.Find("Hud").GetComponent<HudController>().AmmoCount.text = Ammo + "/" + MaxAmmo;
        }

        public void Execute() {
            if (!(Time.time > _lastShot + 1 / FireRate)) {
                return;
            }
            _lastShot = Time.time;

            if (Ammo == 0) {
                return;
            }
            Ammo -= AmmoUse;

            if (_audioSource != null && ShootSound != null) {
                _audioSource.clip = ShootSound;
                _audioSource.PlayOneShot(_audioSource.clip);
            }

            for (var i = 0; i < BulletsPerShot; i++) {
                var direction = Camera.main.transform.forward;
                direction.x += Random.Range(-Spread, Spread);
                direction.y += Random.Range(-Spread, Spread);
                direction.z += Random.Range(-Spread, Spread);

                RaycastHit hit;
                if (!Physics.Raycast(Camera.main.transform.position, direction, out hit)) continue;

                var target = hit.transform.gameObject;
                var playerComponent = target.GetComponent<PlayerComponent>();
                if (playerComponent != null) {
                    playerComponent.Damage(Damage);
                }

                var hitSound = MatchInitialiser.Instance.HitSound;
                if (_audioSource != null && hitSound != null) {
                    _audioSource.clip = hitSound;
                    _audioSource.PlayOneShot(_audioSource.clip);
                }

                if (HitParticle == null) continue;

                var hitParticleObject =
                    Instantiate(HitParticle, hit.point, Quaternion.Euler(Vector3.zero)) as GameObject;
                if (hitParticleObject != null) {
                    var particles = hitParticleObject.GetComponent<ParticleSystem>();
                    particles.Play();
                }
                StartCoroutine("StopParticle", hitParticleObject);
            }
        }

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private IEnumerator StopParticle(GameObject particle) {
            yield return new WaitUntil(() => !particle.GetComponent<ParticleSystem>().isPlaying);
            Destroy(particle);
        }

        private IEnumerator Reload() {
            yield return new WaitUntil(() => !_audioSource.isPlaying);
            Ammo = MaxAmmo;
        }
    }
}