﻿using System.Collections;
using Fortify.BuildSystem;
using UnityEngine;

namespace Fortify.Abilities.Weapons {
    public class Axe : MonoBehaviour, IAbilityExecutor {
        public float Damage;
        public float Range;
        public float SwingRate;
        public AudioClip HitSound;
        public AudioClip SwingSound;

        private AudioSource _audioSource;
        private float _lastSwing;

        private void Start() {
            _audioSource = GetComponent<AudioSource>();
            if (_audioSource != null) {
                _audioSource.playOnAwake = false;
            }
        }

        public void Execute() {
            if (!(Time.time > _lastSwing + 1 / SwingRate)) {
                return;
            }
            _lastSwing = Time.time;

            RaycastHit hit;
            var direction = Camera.main.transform.forward;
            if (Physics.Raycast(Camera.main.transform.position, direction, out hit, Range)) {
                var target = hit.transform.gameObject;

                var tree = target.GetComponent<TreeController>();
                if (tree == null) {
                    return;
                }

                if (_audioSource == null || HitSound == null) return;
                _audioSource.clip = HitSound;
                _audioSource.PlayOneShot(_audioSource.clip);

                if (tree.Health - Damage > 0) {
                    tree.Health -= Damage;
                } else {
                    StartCoroutine("Chop", tree);
                }
            } else {
                if (_audioSource == null || SwingSound == null) return;
                _audioSource.clip = SwingSound;
                _audioSource.PlayOneShot(_audioSource.clip);
            }
        }

        private IEnumerator Chop(TreeController tree) {
            yield return new WaitUntil(() => !_audioSource.isPlaying);
            tree.Chop();
        }
    }
}