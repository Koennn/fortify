﻿using UnityEngine;

namespace Fortify.Abilities {
    public class Ability : MonoBehaviour {
        public string Name;
        public float Cooldown;
        public string Hotkey;
        public KeyCode Key;
        public Sprite Icon;

        [HideInInspector] public IAbilityExecutor Executor;

        private int _currentCooldown;

        private void Start() {
            Executor = GetComponent<IAbilityExecutor>();
        }

        private void FixedUpdate() {
            if (Input.GetKeyDown(Key)) {
                Execute();
            }
        }

        public void Execute() {
            if (InputManager.Unlocked) {
                Executor.Execute();
            }
        }
    }
}