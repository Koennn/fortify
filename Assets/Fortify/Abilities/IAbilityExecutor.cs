﻿namespace Fortify.Abilities {
    public interface IAbilityExecutor {
        void Execute();
    }
}