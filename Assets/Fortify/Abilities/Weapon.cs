﻿using UnityEngine;

namespace Fortify.Abilities {
    public class Weapon : MonoBehaviour {
        public string Name;
        public float Cooldown;
        public bool Automatic;
        public GameObject Prefab;
        public Sprite Icon;

        [HideInInspector] public IAbilityExecutor Executor;

        private int _currentCooldown;

        private void Start() {
            Executor = GetComponent<IAbilityExecutor>();
        }

        private void Update() {
            if (Automatic) {
                if (Input.GetKey(KeyCode.Mouse0)) {
                    Execute();
                }
            } else {
                if (Input.GetKeyDown(KeyCode.Mouse0)) {
                    Execute();
                }
            }
        }

        public void Execute() {
            if (InputManager.Unlocked) {
                Executor.Execute();
            }
        }
    }
}