﻿using UnityEngine;

namespace Fortify.Abilities.Executors {
    public class TestAbility1 : MonoBehaviour, IAbilityExecutor {
        private void Start() { }

        public void Execute() {
            Debug.Log("Executing TestAbility1");
        }
    }
}